import { useAuth0 } from '@auth0/auth0-react'
import { cibFacebook, cibGithub, cibGmail, cibLinkedin } from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import {
  CButton,
  CDropdownDivider,
  CFormInput,
  CInputGroup,
  CInputGroupText,
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
} from '@coreui/react'
import React, { useState } from 'react'

const AppHeaderDropdown = () => {
  const [visibleLogin, setVisibleLogin] = useState(false)
  const { loginWithRedirect } = useAuth0()

  return (
    <>
      <CButton
        className="text-white"
        color="success"
        href="https://md-saqib.github.io/arch-as-diagram-static/"
        target="_blank"
      >
        Upgrade
      </CButton>
      <CButton color="dark" variant="ghost" onClick={() => loginWithRedirect()}>
        Log In
      </CButton>
      <CModal visible={visibleLogin} onClose={() => setVisibleLogin(false)}>
        <CModalHeader>
          <CModalTitle> Login </CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CInputGroup className="mb-3">
            <CInputGroupText id="basic-addon1">Username</CInputGroupText>
            <CFormInput aria-label="Username" aria-describedby="basic-addon1" type="email" />
          </CInputGroup>
          <CInputGroup className="mb-3">
            <CInputGroupText id="basic-addon1">Password</CInputGroupText>
            <CFormInput aria-label="Password" aria-describedby="basic-addon1" type="password" />
          </CInputGroup>
          <div className="d-grid gap-2">
            <CButton color="info" className="text-white">
              Log In
            </CButton>

            <p className="text-center">or use a Social Network</p>
          </div>

          <div className="text-center">
            <CButton color="danger" shape="rounded-pill" className="mx-2">
              <CIcon icon={cibGmail} className="text-white" />
            </CButton>
            <CButton color="info" shape="rounded-pill" className="mx-2">
              <CIcon icon={cibFacebook} className="text-white" />
            </CButton>
            <CButton color="dark" shape="rounded-pill" className="mx-2">
              <CIcon icon={cibGithub} className="text-white" />
            </CButton>
            <CButton color="primary" shape="rounded-pill" className="mx-2">
              <CIcon icon={cibLinkedin} className="text-white" />
            </CButton>
            <br></br>
            <br></br>
            <CDropdownDivider />
            <p>
              Not a member yet? <a href="/#">Sign Up</a>
            </p>
          </div>
        </CModalBody>
      </CModal>
    </>
  )
}
export default AppHeaderDropdown
