import {
  cilActionRedo,
  cilActionUndo,
  cilAddressBook,
  cilCloudUpload,
  cilCode,
  cilCopy,
  cilFile,
  cilFindInPage,
  cilMagnifyingGlass,
  cilMenu,
  cilPencil,
  cilResizeWidth,
  cilShare,
  cilSitemap,
  cilWarning,
  cilZoomIn,
} from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import {
  CButton,
  CContainer,
  CDropdown,
  CDropdownDivider,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CFormInput,
  CFormSelect,
  CHeader,
  CHeaderBrand,
  CHeaderNav,
  CHeaderToggler,
  CModal,
  CModalBody,
  CModalFooter,
  CModalHeader,
  CModalTitle,
  CNavLink,
} from '@coreui/react'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { logo } from 'src/assets/brand/logo'
import { iconCode } from 'src/redux/actions/iconCode'
import { toggleDiagram } from 'src/redux/actions/showDiagram'
import { toggleErrors } from 'src/redux/actions/showErrors'
import { sidebartoggle } from 'src/redux/actions/sidebarToggle'
import { AppHeaderDropdown } from './header/index'

const AppHeader = () => {
  const dispatch = useDispatch()
  const [visibleExport, setVisibleExport] = useState(false)
  const [visibleCopy, setVisibleCopy] = useState(false)
  const [visibleRename, setVisibleRename] = useState(false)

  function clickAction() {
    console.log('clickAction')
  }

  const showFile = async (e) => {
    e.preventDefault()
    const reader = new FileReader()
    reader.onload = async (e) => {
      const file = e.target.result
      dispatch(iconCode(file))
    }
    reader.readAsText(e.target.files[0])
  }

  return (
    <CHeader position="sticky">
      <CContainer fluid>
        <CHeaderToggler
          className="ps-1"
          onClick={() => {
            dispatch(sidebartoggle())
          }}
        >
          <CIcon icon={cilMenu} size="lg" />
        </CHeaderToggler>
        <CHeaderBrand className="mx-auto d-md-none">
          <CIcon icon={logo} height={48} />
        </CHeaderBrand>
        <CHeaderNav className=" d-flex me-auto">
          <CHeaderNav className="ms-3">
            <CDropdown variant="nav-item">
              <CDropdownToggle className="py-0" caret={false}>
                <CNavLink>File</CNavLink>
              </CDropdownToggle>
              <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem role="button" href="Dashboard.js" target="_blank">
                  <CIcon icon={cilFile} className="me-3" />
                  New File
                </CDropdownItem>

                <CDropdownItem role="button" className="position-relative">
                  <input
                    type="file"
                    onChange={(e) => showFile(e)}
                    className="position-absolute top-0 left-0 bottom-0 right-0 w-100 h-100 opacity-0"
                  />
                  <CIcon icon={cilCloudUpload} className="me-3" />
                  Open From Computer
                </CDropdownItem>

                <CDropdownItem onClick={() => setVisibleRename(!visibleRename)} role="button">
                  <CIcon icon={cilPencil} className="me-3" />
                  Rename
                </CDropdownItem>
                <CModal size="lg" visible={visibleRename} onClose={() => setVisibleRename(false)}>
                  <CModalHeader>
                    <CModalTitle> Rename File</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                    <h5>Please enter a new name for the file</h5>
                    <CFormInput type="text" placeholder="File Name" size="lg"></CFormInput>
                  </CModalBody>
                  <br></br>
                  <CModalFooter>
                    <CButton color="success" size="lg" className="text-white">
                      Rename
                    </CButton>
                  </CModalFooter>
                </CModal>

                <CDropdownItem role="button" onClick={() => setVisibleCopy(!visibleCopy)}>
                  <CIcon icon={cilCopy} className="me-3" />
                  Make copy
                </CDropdownItem>
                <CModal size="lg" visible={visibleCopy} onClose={() => setVisibleCopy(false)}>
                  <CModalHeader>
                    <CModalTitle> Copy Document </CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                    <CFormInput type="text" placeholder="Name" aria-label="name" size="lg" />
                  </CModalBody>
                  <br></br>
                  <CModalFooter>
                    <CButton color="success" size="lg" className="text-white">
                      Copy
                    </CButton>
                  </CModalFooter>
                </CModal>

                <CDropdownItem role="button" onClick={() => setVisibleExport(!visibleExport)}>
                  <CIcon icon={cilShare} className="me-3" />
                  Export
                </CDropdownItem>
                <CModal size="lg" visible={visibleExport} onClose={() => setVisibleExport(false)}>
                  <CModalHeader>
                    <CModalTitle> Download</CModalTitle>
                  </CModalHeader>
                  <CModalBody>
                    <h5>Format</h5>
                    <CFormSelect size="lg" className="mb-3" aria-label="Large select example">
                      <option value="png">PNG</option>
                      <option value="high quality png">High Quality PNG</option>
                      <option value="pdf">PDF</option>
                      <option value="svg">SVG</option>
                      <option value="text">Download as Text</option>
                    </CFormSelect>
                  </CModalBody>
                  <CModalFooter>
                    <CButton color="primary" size="lg" className="text-white">
                      Download
                    </CButton>
                  </CModalFooter>
                </CModal>
              </CDropdownMenu>
            </CDropdown>

            <CDropdown variant="nav-item">
              <CDropdownToggle className="py-0" caret={false}>
                <CNavLink>Edit</CNavLink>
              </CDropdownToggle>
              <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem onClick={clickAction} role="button">
                  <CIcon icon={cilMagnifyingGlass} className="me-3" />
                  Find and replace
                </CDropdownItem>

                <CDropdownItem onClick={clickAction} role="button">
                  <CIcon icon={cilActionUndo} className="me-3" />
                  Undo (Ctrl+Z)
                </CDropdownItem>

                <CDropdownItem onClick={clickAction} role="button">
                  <CIcon icon={cilActionRedo} className="me-3" />
                  Redo (Ctrl+Shift+Z)
                </CDropdownItem>
              </CDropdownMenu>
            </CDropdown>

            <CDropdown variant="nav-item">
              <CDropdownToggle className="py-0" caret={false}>
                <CNavLink>View</CNavLink>
              </CDropdownToggle>
              <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem onClick={clickAction} role="button" disabled>
                  <CIcon icon={cilZoomIn} className="me-3" />
                  Zoom actual size
                </CDropdownItem>

                <CDropdownItem onClick={clickAction} role="button" disabled>
                  <CIcon icon={cilResizeWidth} className="me-3" />
                  Zoom to width
                </CDropdownItem>

                <CDropdownItem onClick={clickAction} role="button" disabled>
                  <CIcon icon={cilFindInPage} className="me-3" />
                  Zoom to page
                </CDropdownItem>

                <CDropdownDivider />

                <CDropdownItem
                  onClick={() => {
                    dispatch(sidebartoggle())
                  }}
                  role="button"
                >
                  <CIcon icon={cilCode} className="me-3" />
                  Show help templates
                </CDropdownItem>

                <CDropdownItem
                  onClick={() => {
                    dispatch(toggleErrors())
                  }}
                  role="button"
                >
                  <CIcon icon={cilWarning} className="me-3" />
                  Show errors
                </CDropdownItem>

                <CDropdownItem
                  onClick={() => {
                    dispatch(toggleDiagram())
                  }}
                  role="button"
                >
                  <CIcon icon={cilSitemap} className="me-3" />
                  Show only diagram
                </CDropdownItem>
              </CDropdownMenu>
            </CDropdown>

            <CDropdown variant="nav-item">
              <CDropdownToggle className="py-0" caret={false}>
                <CNavLink>Help</CNavLink>
              </CDropdownToggle>
              <CDropdownMenu className="pt-0" placement="bottom-end">
                <CDropdownItem onClick={clickAction} role="button">
                  <CIcon icon={cilAddressBook} className="me-3" />
                  About Us
                </CDropdownItem>
              </CDropdownMenu>
            </CDropdown>
          </CHeaderNav>
        </CHeaderNav>

        <CHeaderNav className="ms-3">
          <AppHeaderDropdown />
        </CHeaderNav>
      </CContainer>
    </CHeader>
  )
}

export default AppHeader
