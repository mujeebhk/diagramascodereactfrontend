import { CFooter } from '@coreui/react'
import React from 'react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <span className="ms-1">All rights reserved &copy; 2022 </span>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
