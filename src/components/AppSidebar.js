import { cilSearch } from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import { CButton, CSidebar, CSidebarBrand, CSidebarNav } from '@coreui/react'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import SimpleBar from 'simplebar-react'
import 'simplebar/dist/simplebar.min.css'
import { logoNegative } from 'src/assets/brand/logo-negative'
import { sygnet } from 'src/assets/brand/sygnet'
import { AppSidebarNav } from './AppSidebarNav'

const AppSidebar = () => {
  const sidebarShow = useSelector((state) => state.sidebarShow)
  const [allIcons, setAllIcons] = useState([])
  const [searchValue, setSearchValue] = useState('')
  const [searchResults, setSearchResults] = useState([])

  useEffect(() => {
    if (localStorage.length) {
      const parsedResult = JSON.parse(localStorage.getItem('result'))
      setAllIcons(parsedResult)
    } else {
      axios
        .get(`${process.env.REACT_APP_SIDEBAR_API_URL}/api/diagram/resources/icons`)
        .then((response) => {
          const result = response.data.result.platformList
          localStorage.setItem('result', JSON.stringify(result))
          const parsedResult = JSON.parse(localStorage.getItem('result'))
          setAllIcons(parsedResult)
        })
        .catch((err) => {
          alert(err.Message)
        })
    }
  }, [])

  useEffect(() => {
    function recursiveSearch(str) {
      // Get Grandparent and parent keys
      const getGrandParentsKeys = allIcons.map((e, i) => Object.keys(allIcons[i])[0])
      const getParentsKeys = []
      for (let i = 0; i < allIcons.length; i++) {
        const arr = Object.values(allIcons[i]).pop()
        for (let k = 0; k < arr.length; k++) {
          getParentsKeys.push(Object.keys(arr[k])[0])
        }
      }

      // search string in Grandparents
      const searchGrandParent = allIcons.map((e) => {
        return Object.keys(e)
          .filter((key) => key.includes(str))
          .reduce((cur, key) => {
            return Object.assign(cur, { [key]: e[key] })
          }, {})
      })
      const filteredGrandParent = searchGrandParent.filter(
        (value) => Object.keys(value).length !== 0,
      )

      // search string in Parents
      const searchParent = []
      for (let i = 0; i < allIcons.length; i++) {
        const arr = Object.values(allIcons[i]).pop()
        const keyy = getGrandParentsKeys[i]
        arr.map((e) => {
          return searchParent.push({
            [keyy]: [
              Object.keys(e)
                .filter((key) => key.includes(str))
                .reduce((cur, key) => {
                  return Object.assign(cur, { [key]: e[key] })
                }, {}),
            ],
          })
        })
      }

      const clearEmpties = (o) => {
        for (var k in o) {
          if (!o[k] || typeof o[k] !== 'object') {
            continue
          }

          clearEmpties(o[k])
          if (Object.keys(o[k]).length === 0) {
            delete o[k]
          }
        }
        return o
      }

      const filteredParent = clearEmpties(searchParent).filter((n) => n)

      // search string in Children
      const myFind = (n, hs) => {
        const foundIt = hs.find((ob) => 'name' in ob && ob.name.includes(n))
        let res = []
        if (foundIt) {
          res.push(foundIt)
          return res
        } else {
          let ires = []

          hs.forEach((ob) => {
            Object.entries(ob)
              .filter(([k, v]) => v && Array.isArray(v))
              .forEach(([k, v]) => {
                const ores = myFind(n, v)

                if (ores && ores.length > 0) {
                  ires.push({ [k]: ores })
                }
              })
          })
          if (ires.length > 0) return ires
        }
        return false
      }

      const filteredChild = myFind(str, allIcons)

      if (filteredGrandParent.length > 0) {
        setSearchResults(filteredGrandParent)
      } else if (filteredParent.length > 0) {
        setSearchResults(filteredParent)
      } else if (filteredChild) {
        setSearchResults(filteredChild)
      } else {
        setSearchResults([])
      }
    }
    recursiveSearch(searchValue)

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchValue])

  const searchHandle = (e) => {
    const inputSearch = e.target.value
    setSearchValue(inputSearch)
  }

  return (
    <CSidebar position="fixed" visible={sidebarShow}>
      <CSidebarBrand className="d-none d-md-flex" to="/">
        <CIcon className="sidebar-brand-full" icon={logoNegative} height={35} />
        <CIcon className="sidebar-brand-narrow" icon={sygnet} height={35} />
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          <div className="m-2">
            <div className="input-group mb-3">
              <input
                type="text"
                className="form-control shadow-none"
                placeholder="Search"
                aria-label="Search"
                value={searchValue}
                onChange={searchHandle}
              />
              <CButton type="button" color="secondary" variant="outline">
                <CIcon icon={cilSearch} size="lg" />
              </CButton>
            </div>
          </div>
          <AppSidebarNav items={searchValue ? searchResults : allIcons} />
        </SimpleBar>
      </CSidebarNav>
    </CSidebar>
  )
}

export default React.memo(AppSidebar)
