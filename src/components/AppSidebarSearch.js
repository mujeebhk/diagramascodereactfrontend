import { CImage } from '@coreui/react'
import React from 'react'

export const AppSidebarSearch = ({ items }) => {
  const urls = (function findAllByKey(obj, keyToFind) {
    return Object.entries(obj).reduce(
      (acc, [key, value]) =>
        key === keyToFind
          ? acc.concat(value)
          : typeof value === 'object'
          ? acc.concat(findAllByKey(value, keyToFind))
          : acc,
      [],
    )
  })(items, 'url')

  const code = (function findAllByKey(obj, keyToFind) {
    return Object.entries(obj).reduce(
      (acc, [key, value]) =>
        key === keyToFind
          ? acc.concat(value)
          : typeof value === 'object'
          ? acc.concat(findAllByKey(value, keyToFind))
          : acc,
      [],
    )
  })(items, 'code')

  function onclick(code) {
    return alert(`You clicked ${code}`)
  }

  return (
    <div className="container">
      <div className="row justify-content-center align-items-center">
        {urls.map((url, i) => {
          return (
            <div key={i} className="col-3 cursor-pointer">
              <CImage
                key={i}
                rounded
                className="w-100 pb-3"
                role="button"
                src={`${process.env.REACT_APP_SIDEBAR_API_URL}${url}`}
                onClick={() => onclick(code[i])}
              />
            </div>
          )
        })}
      </div>
    </div>
  )
}
