import {
  CAccordion,
  CAccordionBody,
  CAccordionHeader,
  CAccordionItem,
  CImage,
  CTooltip,
} from '@coreui/react'
import React from 'react'
import { useDispatch } from 'react-redux'
import { iconCode } from 'src/redux/actions/iconCode'

export const AppSidebarNav = ({ items }) => {
  const dispatch = useDispatch()

  function getInnerItems(ele) {
    const itemNames = ele[Object.keys(ele)[0]].map((e, i) => {
      return (
        <div key={i} className="col-3 cursor-pointer">
          <CTooltip
            content={
              <div>
                <img
                  alt={e.name}
                  height="100"
                  src={`${process.env.REACT_APP_SIDEBAR_API_URL}${e.url}`}
                  className="bg-white p-1"
                />
                <p>{e.name}</p>
              </div>
            }
            placement="auto"
          >
            <CImage
              rounded
              className="w-100 p-1 mb-3 bg-white"
              role="button"
              src={`${process.env.REACT_APP_SIDEBAR_API_URL}${e.url}`}
              onClick={() => dispatch(iconCode(e.code))}
            />
          </CTooltip>
        </div>
      )
    })
    return itemNames
  }

  function getNestedItems(id) {
    const arr = Object.values(items[id]).pop()
    return arr.map((e, i) => {
      return (
        <CAccordion key={i}>
          <CAccordionItem itemKey={i} style={{ backgroundColor: '#232c3b' }}>
            <CAccordionHeader>{Object.keys(arr[i])}</CAccordionHeader>
            <CAccordionBody>
              <div className="row justify-content-center align-items-center">
                {getInnerItems(e)}
              </div>
            </CAccordionBody>
          </CAccordionItem>
        </CAccordion>
      )
    })
  }

  return items.map((key, idx) => {
    return (
      <React.Fragment key={idx}>
        <CAccordion>
          <CAccordionItem itemKey={idx}>
            <CAccordionHeader>{Object.keys(items[idx])}</CAccordionHeader>
            <CAccordionBody className="p-0">{getNestedItems(idx)}</CAccordionBody>
          </CAccordionItem>
        </CAccordion>
      </React.Fragment>
    )
  })
}
