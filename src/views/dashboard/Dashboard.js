import axios from 'axios'
import { cilWarning } from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import { CAlert, CButton } from '@coreui/react'
import Editor from '@monaco-editor/react'
import { Resizable } from 're-resizable'
import React, { useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toggleErrors } from 'src/redux/actions/showErrors'

const Dashboard = () => {
  const dispatch = useDispatch()
  const iconCode = useSelector((state) => state.iconCode)
  const showErrors = useSelector((state) => state.showErrors)
  const showDiagram = useSelector((state) => state.showDiagram)
  const [imageSrc, setImageSrc] = useState('')

  const monacoRef = useRef(null)

  function handleEditorDidMount(editor, monaco) {
    monacoRef.current = editor
    editor.setSelection(new monaco.Selection(1, 23, 1, 23))
    monacoRef.current.focus()
  }

  function appendCode(value, event) {
    var selection = monacoRef.current.getSelection()
    var id = { major: 1, minor: 1 }
    var text = `\n${value}`
    var op = { identifier: id, range: selection, text: text, forceMoveMarkers: true }
    monacoRef.current.executeEdits('source-update', [op])
  }

  function generate() {
    console.log("generate method called");
    const data = { code: monacoRef.current.getValue() };

    axios.post(`${process.env.REACT_APP_SIDEBAR_API_URL}/api/diagram/diagram/generate`, data, {
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(response => {
        console.log(response.data);
        setImageSrc(response.data.result.diagram)
        // Handle successful response
      })
      .catch(error => {
        console.log('Error:', error);
        // Handle error
      });
  }

  useEffect(() => {
    if (iconCode) {
      appendCode(iconCode)
    }
  }, [iconCode])

  return (
    <>
      <div className="d-flex">
        {showDiagram ? (
          <Resizable
            defaultSize={{
              width: '50%',
            }}
          >

<div style={{ display: 'flex', justifyContent: 'center' }}>
             <CButton color="success" onClick={generate}>Execute</CButton>
      </div>
            
            <Editor
              height="calc(100vh - 113px)"
              theme="vs-dark"
              language="python"
              defaultValue="# Write you code below"
              onMount={handleEditorDidMount}
            />


          </Resizable>
        ) : (
          <></>
        )}
        <div className="container-fluid">
          <div className="d-flex flex-column" style={{ height: 'calc(100vh - 113px)' }}>
            <h2 className="flex-grow-1">Output</h2>
            <img src={imageSrc} alt="Diagram Output" width="600" height="600"></img>
            <CAlert
              color="danger"
              visible={showErrors}
              className="mt-auto w-100 d-flex align-items-center"
              dismissible
              onClose={() => {
                dispatch(toggleErrors())
              }}
            >
              <CIcon icon={cilWarning} size="lg" /> &nbsp;
              <div>Error</div>
            </CAlert>
          </div>
        </div>
      </div>
    </>
  )
}

export default Dashboard
