import { combineReducers, createStore } from 'redux'
import iconCodeReducer from '../reducers/iconCode'
import showDiagramReducer from '../reducers/showDiagram'
import ShowErrorReducer from '../reducers/showErrors'
import sidebarShowReducer from '../reducers/sidebarToggle'

const configureStore = () => {
  const store = createStore(
    combineReducers({
      sidebarShow: sidebarShowReducer,
      iconCode: iconCodeReducer,
      showErrors: ShowErrorReducer,
      showDiagram: showDiagramReducer,
    }),
  )
  return store
}

export default configureStore
