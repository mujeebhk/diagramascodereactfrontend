export const iconCode = (code) => {
  return {
    type: 'CLICK',
    payload: code,
  }
}
