const iconCodeInitialState = ''

const iconCodeReducer = (state = iconCodeInitialState, action) => {
  if (action.type === 'CLICK') {
    return action.payload
  }
  return state
}

export default iconCodeReducer
