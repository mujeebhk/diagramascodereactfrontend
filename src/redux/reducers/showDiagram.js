const ShowDiagramInitialState = true

const showDiagramReducer = (state = ShowDiagramInitialState, action) => {
  if (action.type === 'TOGGLE') {
    return !state
  }
  return state
}
export default showDiagramReducer
