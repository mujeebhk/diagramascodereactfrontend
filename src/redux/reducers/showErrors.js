const ShowErrorsInitialState = false

const ShowErrorReducer = (state = ShowErrorsInitialState, action) => {
  if (action.type === 'TOGGLE_ERRORS') {
    return !state
  }
  return state
}

export default ShowErrorReducer
